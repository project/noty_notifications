<?php

/**
 * @file
 * noty_notifications.
 * A noty based notification system.
 */

/**
 * Define Default Values
 */

define('NOTY_NOTIFICATIONS_LAYOUT_DEFAULT', 'topRight');

// Layouts
define('NOTY_NOTIFICATIONS_LAYOUT_INLINE', 'inline');

define('NOTY_NOTIFICATIONS_LAYOUT_TOP', 'top');
define('NOTY_NOTIFICATIONS_LAYOUT_TOP_LEFT', 'topLeft');
define('NOTY_NOTIFICATIONS_LAYOUT_TOP_CENTER', 'topCenter');
define('NOTY_NOTIFICATIONS_LAYOUT_TOP_RIGHT', 'topRight');

define('NOTY_NOTIFICATIONS_LAYOUT_CENTER', 'center');
define('NOTY_NOTIFICATIONS_LAYOUT_CENTER_LEFT', 'centerLeft');
define('NOTY_NOTIFICATIONS_LAYOUT_CENTER_RIGHT', 'centerRight');

define('NOTY_NOTIFICATIONS_LAYOUT_BOTTOM', 'bottom');
define('NOTY_NOTIFICATIONS_LAYOUT_BOTTOM_LEFT', 'bottomLeft');
define('NOTY_NOTIFICATIONS_LAYOUT_BOTTOM_CENTER', 'bottomCenter');
define('NOTY_NOTIFICATIONS_LAYOUT_BOTTOM_RIGHT', 'bottomRight');

// Available notifications types
define('NOTY_NOTIFICATIONS_TYPE_NONE', 'none');
define('NOTY_NOTIFICATIONS_TYPE_ALERT', 'alert');
define('NOTY_NOTIFICATIONS_TYPE_WARNING', 'warning');
define('NOTY_NOTIFICATIONS_TYPE_ERROR', 'error');
define('NOTY_NOTIFICATIONS_TYPE_INFORMATION', 'information');
define('NOTY_NOTIFICATIONS_TYPE_SUCCESS', 'success');

/**
 * Implements hook_permission().
 */
function noty_notifications_permission() {
  return array(
    'view noty notifications' => array(
      'title' => t('View noty notifications'),
    ),
    'delete noty notification' => array(
      'title' => t('Delete noty notification'),
    ),
    'administer noty notification' => array(
      'title' => t('Administer noty notifications'),
    ),
  );
}

/**
 * Implements hook_help().
 */
function noty_notifications_help($path, $arg) {
  if ($path == 'admin/config/user-interface/noty-notifications') {
    return t('Here you can configure your notifications.');
  }
}

/**
 * Implements hook_menu().
 */
function noty_notifications_menu() {
  $items = array();

  $items['admin/config/user-interface/noty-notifications'] = array(
    'title' => 'Noty Notifications',
    'description' => 'Settings to control noty notifications',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('noty_notifications_admin_settings'),
    'access arguments' => array('administer noty notifications'),
    'file' => 'includes/noty_notifications.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['noty/js/%'] = array(
    'page callback' => 'noty_notifications_js_notifications',
    'page arguments' => array(2),
    'access arguments' => array(2),
    'access callback' => 'noty_notifications_access',
    'file' => 'includes/noty_notifications.ajax.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['noty/js/%/%'] = array(
    'page callback' => 'noty_notifications_js_notifications',
    'page arguments' => array(2, 3),
    'access arguments' => array(2),
    'access callback' => 'noty_notifications_access',
    'file' => 'includes/noty_notifications.ajax.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Check access to operations.
 *
 * @param type $op
 * @return boolean
 */
function noty_notifications_access($op) {
  switch ($op) {
    case 'delete':
      return user_access('delete noty notification');
    case 'get':
    case 'count':
      return user_access('view noty notifications');
  }

  return TRUE;
}

/**
 * Add new notification to database.
 *
 * @param type $noty_message
 *   Notification text.
 * @param type $variables
 *   Variables for replace in notification text.
 * @param type $noty_type
 *   Type of notification.
 * @param type $removable
 *   Is this notification removable from database?
 * @return type
 *   Return notification object.
 */
function noty_notifications_add_notification_to_database($uid, $noty_message, $variables = array(), $noty_type = NOTY_NOTIFICATIONS_TYPE_INFORMATION, $removable = 1) {
  return NotyNotifications::addNotificationToDatabase($uid, $noty_message, $variables, $noty_type, $removable);
}

/**
 * Add new notification "on the fly".
 *
 * @param type $noty_message
 *   Notification text.
 * @param type $variables
 *   Variables for replace in notification text.
 * @param type $noty_type
 *   Type of notification.
 * @return type
 *   Return notification object.
 */
function noty_notifications_add_notification($noty_message, $variables = array(), $noty_type = NOTY_NOTIFICATIONS_TYPE_INFORMATION) {
  return NotyNotifications::addNotification($noty_message, $variables, $noty_type);
}

function noty_notifications_preprocess_page(&$vars) {
  global $user;

  // @@@ToDo: problem with conflicting JS on some admin pages (Rules UI AJAX,
  //          Context UI AJAX)
  // error:
  // TypeError: this.options.layout is undefined
  // this.options = $.extend({}, this.options, this.options.layout.options);

  if (arg(0) == 'admin') return;

  $module_path = drupal_get_path('module', 'noty_notifications');
  $lib_path = libraries_get_path('noty');
  drupal_add_js($lib_path . '/js/noty/promise.js');
  drupal_add_js($lib_path . '/js/noty/jquery.noty.js');
  drupal_add_js($lib_path . '/js/noty/themes/default.js');
  drupal_add_js($lib_path . '/js/noty/layouts/' . NOTY_NOTIFICATIONS_LAYOUT_TOP_RIGHT . '.js');

  $noty = new NotyNotifications;

  /** testing !!! **/
//  $available_types = array(
//    NOTY_NOTIFICATIONS_TYPE_ALERT,
//    NOTY_NOTIFICATIONS_TYPE_WARNING,
//    NOTY_NOTIFICATIONS_TYPE_ERROR,
//    NOTY_NOTIFICATIONS_TYPE_INFORMATION,
//    NOTY_NOTIFICATIONS_TYPE_SUCCESS,
//  );
//
//  $query = db_select('noty_notifications', 'n');
//  $query->addExpression('COUNT(n.nid)', 'count');
//  $query->condition('n.uid', $user->uid);
//  $count = $query->execute()->fetchField();
//  $create_notifications = 3;
//  if ($count <= 3) {
//    for ($i = 1; $i <= $create_notifications - $count; $i++) {
//      $type = $available_types[array_rand($available_types)];
//      noty_notifications_save_notification($user->uid, $i + $count .' test message...  type = '. $type, array(), $type);
//    }
//  }

  noty_notifications_add_notification('This message is here for testing. This message is added "on the fly". There is rule which store message to database after user create new content. (type = '. NOTY_NOTIFICATIONS_TYPE_ALERT. ')', array(), NOTY_NOTIFICATIONS_TYPE_ALERT);
  /** testing !!! **/

  drupal_add_js(array(
    'notyNotifications' => array(
      'notifications' => $noty->getNotificationsJS()
    )), 'setting');
  drupal_add_js($module_path . '/js/noty.js');
}

//  $.noty.defaults = {
//    layout: 'top',
//    theme: 'default',
//    type: 'alert',
//    text: '',
//    dismissQueue: true, // If you want to use queue feature set this true
//    template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
//    animation: {
//      open: {height: 'toggle'},
//      close: {height: 'toggle'},
//      easing: 'swing',
//      speed: 500 // opening & closing animation speed
//    },
//    timeout: false, // delay for closing event. Set false for sticky notifications
//    force: false, // adds notification to the beginning of queue when set to true
//    modal: false,
//    closeWith: ['click'], // ['click', 'button', 'hover']
//    callback: {
//      onShow: function() {},
//      afterShow: function() {},
//      onClose: function() {},
//      afterClose: function() {}
//    },
//    buttons: false // an array of buttons
//  };
//}

function noty_notifications_get_types() {
  return array(
    NOTY_NOTIFICATIONS_TYPE_ALERT => 'alert',
    NOTY_NOTIFICATIONS_TYPE_WARNING => 'warning',
    NOTY_NOTIFICATIONS_TYPE_ERROR => 'error',
    NOTY_NOTIFICATIONS_TYPE_INFORMATION => 'information',
    NOTY_NOTIFICATIONS_TYPE_SUCCESS => 'success',
  );
}

/**
 * Class defines an storage pool for Noty notifications.
 */
class NotyStorage {
  private static $instance;

  // Notifications storage.
  private $notifications = array();

  /**
   * constructor
   */
  private function __construct() {
  }

  /**
   * getInstance
   *
   * @static
   * @access public
   * @return object NotyStorage instance
   */
  public static function getInstance() {
    if ( !(self::$instance instanceof NotyStorage) ) {
      self::$instance = new NotyStorage();
    }

    return self::$instance;
  }

  public static function addNotification(NotyNotification $notification) {
    NotyStorage::getInstance()->notifications[] = $notification;

    return $notification;
  }

  /**
   * Return array of notifications objects.
   *
   * @return array
   *   Array of notifications objects.
   */
  public static function getNotifications() {
    return NotyStorage::getInstance()->notifications;
  }

  public static function countNotifications() {
    return count(NotyStorage::getInstance()->notifications);
  }
}